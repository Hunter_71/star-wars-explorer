from typing import Dict, List, Optional

from django.shortcuts import render

from sw_explorer.logic import LIMIT, data_headers, fetch_characters, get_characters, store_dataset
from sw_explorer.models import Dataset

CSFR_TOKEN_KEY = "csrfmiddlewaretoken"
AGGREGATION_FIELDS_KEY = "aggregation_fields"

# TODO: If the app will grow to cover more functionalities, it will be reasonable to switch views implementation
#       to [DRF](https://www.django-rest-framework.org/) format. It would also force following REST API standards.


def home(request, error: Optional[str] = None, warning: Optional[str] = None):
    context = {
        "data": Dataset.get_all_ordered(),
        "error": error,
        "warning": warning,
    }
    return render(request, "index.html", context=context)


def fetch(request):
    data, error, warning = fetch_characters()
    if data and not error and not warning:
        error = store_dataset(data)

    return home(request, error=error, warning=warning)


def dataset(request, dataset_id: int):
    if not (metadata := Dataset.get_or_none(dataset_id)):
        return home(request, warning="Selected dataset is not available")

    counter = 1
    if request.method == "POST":
        # TODO: Loading more data on button click may be optimised by lazy loading.
        #   Current solution with sending POST method every time we request more data is reasonably efficient.
        #   However, it can be significantly slower once our dataset will have more columns, thousands of rows
        #   or simply with necessity of handling way more requests (eg. due to multiple users).
        #   In that case, we can optimise by lazy loading the increment of records into the application.
        #   Then, it won't be needed anymore to reload the whole collection page
        #   or render the same records multiple times.
        counter = int(request.POST.get("counter", 0)) + 1

    data, error = get_characters(dataset_name=metadata.name, limit=LIMIT * counter)
    if error:
        return home(request, error=error)

    context = {
        "data": data,
        "dataset_id": dataset_id,
        "dataset_name": metadata.name,
        "headers": data_headers(),
        "counter": counter,
        "load_more": True,
    }

    return render(request, "dataset.html", context=context)


def aggregate(request, dataset_id: int):
    if not (metadata := Dataset.get_or_none(dataset_id)):
        return home(request, warning="Selected dataset is not available")

    aggregation_fields = []
    if request.method == "POST":
        aggregation_fields = _resolve_aggregation_fields(request.POST.copy())

    if not aggregation_fields:
        return dataset(request, dataset_id)

    data, error = get_characters(dataset_name=metadata.name, aggregation_fields=aggregation_fields)
    if error:
        return home(request, error=error)

    context = {
        "data": data,
        "dataset_id": dataset_id,
        "dataset_name": metadata.name,
        "headers": data_headers(),
        "aggregation_fields": aggregation_fields,
    }

    return render(request, "dataset.html", context=context)


def _resolve_aggregation_fields(data: Dict) -> List[str]:
    aggregation_fields = [x for x in data[AGGREGATION_FIELDS_KEY].split(",") if x]

    for key, value in data.items():
        if key not in (AGGREGATION_FIELDS_KEY, CSFR_TOKEN_KEY):
            was_selected = bool(int(value)) and key in aggregation_fields
            if was_selected:
                aggregation_fields.remove(key)
            else:
                aggregation_fields.append(key)
            break

    return aggregation_fields
