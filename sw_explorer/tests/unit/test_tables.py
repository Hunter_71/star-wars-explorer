from datetime import datetime

from sw_explorer.tables import CharactersTable


def test_dataset_transformation(mock_characters_dataset, mock_planet_data, mock_characters_data):
    assert mock_characters_dataset.fieldnames == CharactersTable.HEADERS

    results = []
    for row in CharactersTable(mock_characters_data).transform(mock_planet_data)[1:]:
        results.append({k: v for k, v in zip(CharactersTable.HEADERS, row)})

    for character in mock_characters_dataset:
        character["date"] = datetime.strptime(character["date"], "%Y-%m-%d").date()
        if character["homeworld"] == "null":
            character["homeworld"] = None
        assert character in results
