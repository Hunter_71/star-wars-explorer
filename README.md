# Star Wars Explorer
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)
[![Checked with mypy](http://www.mypy-lang.org/static/mypy_badge.svg)](http://mypy-lang.org/)

Simple application that allows to fetch and explore Star Wars characters dataset from https://swapi.dev/
and do some trivial aggregations against properties.

## Run application

### Run application using Docker

Simply run below commands to build and run application:

    docker build .
    docker compose up app

You should see below info in the console:

    Django version 3.2.18, using settings 'star_wars_explorer.settings'
    Starting development server at http://0.0.0.0:8000/
    Quit the server with CONTROL-C.

Then simply open the above localhost url into your browser to play with the application.

### Run application locally (via poetry)

Note: Before following below steps, make sure you have properly installed [`poetry`](https://python-poetry.org/docs/#installation) package.

Installation can be performed by running below command:

    poetry install

Then running application is pretty simple. Just run below command:

    ./bin/run-server

You should see below info in the console:

    Django version 3.2.18, using settings 'star_wars_explorer.settings'
    Starting development server at http://0.0.0.0:8000/
    Quit the server with CONTROL-C.

Then simply open the above localhost url into your browser to play with the application.

### Post scriptum

Note, that you'll see below output in the console while running the application:

    You have 18 unapplied migration(s). Your project may not work properly until you apply the migrations for app(s): admin, auth, contenttypes, sessions.
    Run 'python manage.py migrate' to apply them.

This is only visible, because we are running application using Django development server.
Mentioned migrations were additionally omitted to keep the Sqlite DB as small as possible.
Please note, that those migrations are NOT REQUIRED for application to work correctly.
    
## Run tests

Note: Please keep in mind, that tests are also running in the Gitlab CI pipeline.

### Run tests using Docker

Simply run below commands to build and run test suites via Docker

    docker build .
    docker compose up unit integration

### Run tests locally (via poetry)

First make sure the project was installed locally as described in `Run application locally (via poetry)` section.
Then run one of below command locally to run unit or integration tests: 

    ./bin/run-tests-unit
    ./bin/run-tests-integration

## Check codestyle

Note: Please keep in mind, that codestyle checker is also running in the Gitlab CI pipeline.

### Check codestyle using Docker

Simply run below commands to build and run test suites via Docker

    docker build .
    docker compose up codestyle

If codestyle is ok, you should see the below final message in the console output. Otherwise, proper error will be displayed.

    👏 Good job! Codestyle is awesome 👌

### Run tests locally (via poetry)

First make sure the project was installed locally as described in `Run application locally (via poetry)` section.
Then run below command locally to perform codestyle check: 

    ./bin/run-codestyle-check

If codestyle is ok, you should see the below final message in the console output. Otherwise, proper error will be displayed.

    👏 Good job! Codestyle is awesome 👌

### Ideas for improvements

Example additional functionalities to cover in the future:
 - New search to filter out datasets by name
 - Possibility to delete fetched collections (both from database and physically from filesystem)
 - "Load more" button for aggregations
 - "Load more" button disappears once all rows are displayed on screen
 - New "Clear" button to reset all selected columns for aggregations
 - Handle fetching different datasets from Star Wars API

Also, there is a number of improvement suggestions mentioned in the code as `TODOs`.
