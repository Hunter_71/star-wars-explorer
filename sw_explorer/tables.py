from datetime import datetime
from typing import Dict, List

from petl import Table


class CharactersTable(Table):
    HEADERS = [
        "name",
        "height",
        "mass",
        "hair_color",
        "skin_color",
        "eye_color",
        "birth_year",
        "gender",
        "homeworld",
        "date",
    ]

    def __init__(self, data: List[Dict[str, str]]):
        if not data:
            raise ValueError("Dataset cannot be empty!")

        self._data = data

    def __iter__(self):
        header = tuple(self._data[0].keys())
        yield header

        for row in self._data:
            yield tuple(row.values())

    def transform(self, planets: Dict[str, str]) -> "CharactersTable":
        return (
            self
            # create date field from "edited" property
            .addfield("date", lambda row: datetime.strptime(row.edited, "%Y-%m-%dT%H:%M:%S.%fZ").date())
            # replace planet resource url with actual planet name
            .convert("homeworld", lambda resource_url: planets.get(resource_url, None))
            # cut data to relevant properties
            .cut(self.HEADERS)
        )
