from django.urls import path

from sw_explorer import views

urlpatterns = [
    path("", views.home, name="home"),
    path("datasets/fetch", views.fetch, name="fetch_datasets"),
    path("datasets/<int:dataset_id>", views.dataset, name="list_datasets"),
    path("datasets/<int:dataset_id>/aggregate", views.aggregate, name="aggregate_datasets"),
]
