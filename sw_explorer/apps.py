from django.apps import AppConfig


class SwExplorerConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "sw_explorer"
