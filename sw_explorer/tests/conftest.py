import json
from csv import DictReader
from typing import Dict, List

import pytest
import requests_mock

from star_wars_explorer.settings import BASE_DIR
from sw_explorer.clients.swapi import BASE_URL, CHARACTERS_ENDPOINT, PLANETS_ENDPOINT


@pytest.fixture
def mock_planet_endpoint():
    url = f"{BASE_URL}/{PLANETS_ENDPOINT}"
    with open(f"{BASE_DIR}/sw_explorer/tests/data/planets.json") as f:
        resp = json.load(f)

    with requests_mock.Mocker() as m:
        m.get(url, json=resp)
        yield


@pytest.fixture
def mock_characters_endpoint():
    url = f"{BASE_URL}/{CHARACTERS_ENDPOINT}"
    with open(f"{BASE_DIR}/sw_explorer/tests/data/people_1.json") as f:
        resp_page_1 = json.load(f)
    with open(f"{BASE_DIR}/sw_explorer/tests/data/people_2.json") as f:
        resp_page_2 = json.load(f)

    with requests_mock.Mocker() as m:
        m.get(url, json=resp_page_1)
        m.get(f"{url}?page=1", json=resp_page_1)
        m.get(f"{url}?page=2", json=resp_page_2)
        yield


@pytest.fixture
def mock_planet_data() -> Dict[str, str]:
    with open(f"{BASE_DIR}/sw_explorer/tests/data/planets.json") as f:
        data = json.load(f).get("results")

    return {record.get("url"): record.get("name") for record in data}


@pytest.fixture
def mock_characters_data() -> List[Dict[str, str]]:
    data = []
    with open(f"{BASE_DIR}/sw_explorer/tests/data/people_1.json") as f:
        data += json.load(f).get("results")
    with open(f"{BASE_DIR}/sw_explorer/tests/data/people_2.json") as f:
        data += json.load(f).get("results")

    return data


@pytest.fixture
def mock_characters_dataset():
    with open(f"{BASE_DIR}/sw_explorer/tests/data/characters_dataset.csv") as f:
        yield DictReader(f)
