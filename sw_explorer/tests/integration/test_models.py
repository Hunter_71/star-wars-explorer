import datetime

import pytest
from django.utils.timezone import make_aware

from sw_explorer.models import Dataset

pytestmark = pytest.mark.django_db


def test_get_or_none():
    # sanity check
    assert Dataset.objects.count() == 0

    metadata = Dataset.objects.create(name="test", created_at=make_aware(datetime.datetime.utcnow()))

    assert Dataset.get_or_none(metadata.id) == metadata


def test_get_or_none_when_missing():
    # sanity check
    assert Dataset.objects.count() == 0
    assert not Dataset.get_or_none(1)


def test_get_all_ordered():
    # sanity check
    assert Dataset.objects.count() == 0

    created_at_last = datetime.datetime.utcnow()
    created_at_first = created_at_last - datetime.timedelta(days=2, hours=4)
    created_at_middle = created_at_last - datetime.timedelta(hours=10)

    metadata_first = Dataset.objects.create(name="test", created_at=make_aware(created_at_first))
    metadata_last = Dataset.objects.create(name="test", created_at=make_aware(created_at_last))
    metadata_middle = Dataset.objects.create(name="test", created_at=make_aware(created_at_middle))

    expected = [metadata_last, metadata_middle, metadata_first]
    assert list(Dataset.get_all_ordered()) == expected
