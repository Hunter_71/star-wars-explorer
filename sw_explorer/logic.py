import logging
from datetime import datetime
from typing import List, Optional, Tuple

from django.utils.timezone import make_aware
from petl import Table, fromcsv
from requests import ConnectionError, HTTPError

from star_wars_explorer.settings import DATASET_DIR
from sw_explorer.clients.swapi import StarWarsAPIClient
from sw_explorer.models import Dataset
from sw_explorer.tables import CharactersTable

logger = logging.getLogger("django")
logger.setLevel(logging.INFO)

swapi_client = StarWarsAPIClient()

# TODO: Below may be turned into project settings/env vars to allow easier customization in future
LIMIT = 10
COLLECTION_NAME_PREFIX = "star_wars_characters"


def fetch_characters() -> Tuple[Optional[Table], Optional[str], Optional[str]]:
    """Fetches characters dataset from SWAPI and transforming it to desired form"""
    data = None
    error = None
    warning = None

    try:
        # TODO: Collecting data from SWAPI
        #  Currently, data is collected in threads to boost the speed, but the potential for future scaling is limited.
        #  Thus, next optimisation step might be to implement a batch job, that would be started by this request,
        #  while displaying some spinner/temporal communicate to the user, informing data is fetching now
        #  and complete dataset would be available as soon as possible.
        #  ----
        #  The decision should vary on the size of source data (SWAPI content).
        #  Having ~80 characters and ~60 planets in official SWAPI makes reasonable to stay with synchronous fetching.

        planets = swapi_client.get_planets()
        characters = swapi_client.get_characters()
    except ValueError as e:
        logger.error(f"Programmatic error, {e=}")
        error = "01 Programmatic Error: Please contact your developer in order to fix the app."
        return data, error, warning
    except (ConnectionError, HTTPError) as e:
        logger.error(f"Error when fetching characters from SWAPI, {e=}")
        warning = "Could not fetch data from SWAPI. Please try again later or check if the service is available."
        return data, error, warning

    if not characters:
        warning = "The characters dataset fetched from SWAPI is empty"
        logger.error(warning)
        return data, error, warning

    planets_mapping = {key: value for p in planets if (key := p.get("url")) and (value := p.get("name"))}
    data = CharactersTable(characters).transform(planets_mapping)
    return data, error, warning


def store_dataset(data: Table) -> Optional[str]:
    """Stores given dataset as CSV file in filesystem and its metadata in database"""
    error = None

    collection_date = datetime.utcnow()
    collection_name = f"{COLLECTION_NAME_PREFIX}_{collection_date.strftime('%Y%m%d_%H%M%S')}.csv"

    # check if collection with given name exists
    if Dataset.objects.filter(name=collection_name).count() > 0:
        # TODO: to improve, we may freeze fetching for fixed time to prevent duplicated requests
        #   (duplicated request may be send eg. on manual page reload)

        # do not store duplicated dataset, keep the original one
        return error

    try:
        data.tocsv(str(DATASET_DIR / collection_name))
        Dataset.objects.create(name=collection_name, created_at=make_aware(collection_date))
    except OSError as e:
        logging.error(f"Could not store collected dataset in a filesystem, {e=}")
        error = "02 OSError: Could not store dataset in the filesystem"
    except Exception as e:
        logger.error(f"Could not store dataset metadata in database, {e=}")
        error = "03 Programmatic Error: Could not store dataset metadata in database"

    return error


def get_characters(
    dataset_name: str, limit: int = LIMIT, aggregation_fields: Optional[List[str]] = None
) -> Tuple[Optional[Table], Optional[str]]:
    """Returns petl Table object with the requested data from characters dataset."""
    error = None

    if not (collection_path := DATASET_DIR / dataset_name).exists():
        logger.error(f"Could not read the dataset from filesystem: file not found")
        return None, "04 OSError: Could not read the dataset from filesystem. File may be corrupted or missing."

    data = fromcsv(collection_path)

    if aggregation_fields:
        data = data.aggregate(aggregation_fields, len)

    return data.head(limit), error


def data_headers() -> List[str]:
    """Returns list of characters dataset headers."""
    return CharactersTable.HEADERS
