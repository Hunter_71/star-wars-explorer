"""
Within this module we actually test if SWAPI dataset is available
and if it can be read by client implementation
"""

from http import HTTPStatus
from typing import Type

import pytest
import requests
from requests import ConnectionError, HTTPError

from sw_explorer.clients.swapi import BASE_URL, CHARACTERS_ENDPOINT, PLANETS_ENDPOINT, StarWarsAPIClient

pytestmark = pytest.mark.integration


def test_swapi_collection_is_available():
    resp = requests.get(BASE_URL)
    assert resp.status_code == HTTPStatus.OK
    assert resp.json()


def test_swapi_get_planets():
    client = StarWarsAPIClient()
    planets = client.get_planets()

    expected = requests.get(f"{BASE_URL}/{PLANETS_ENDPOINT}").json().get("count", -1)
    assert len(planets) == expected


def test_swapi_get_characters():
    client = StarWarsAPIClient()
    characters = client.get_characters()

    expected = requests.get(f"{BASE_URL}/{CHARACTERS_ENDPOINT}").json().get("count", -1)
    assert len(characters) == expected


@pytest.mark.parametrize(
    "url, raised_error",
    (
        ("https://swapi.dev/fakeapi/", HTTPError),
        ("http://swapi.dev/fakeapi/", HTTPError),
        ("https://fake.url", ConnectionError),
        ("http://fake.url", ConnectionError),
    ),
)
def test_incorrect_base_url(url: str, raised_error: Type):
    with pytest.raises(raised_error):
        StarWarsAPIClient(url).get_characters()
