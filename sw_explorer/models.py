from typing import Optional

from django.db import models
from django.db.models import QuerySet


class Dataset(models.Model):
    # TODO: add more dataset metadata if needed
    name = models.CharField(max_length=64)
    created_at = models.DateTimeField()

    @staticmethod
    def get_all_ordered() -> QuerySet:
        return Dataset.objects.all().order_by("-created_at")

    @staticmethod
    def get_or_none(_id: int) -> Optional["Dataset"]:
        return Dataset.objects.filter(id=_id).first()
