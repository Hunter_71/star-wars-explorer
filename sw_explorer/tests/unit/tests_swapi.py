import pytest

from sw_explorer.clients.swapi import StarWarsAPIClient


def test_get_planets(mock_planet_endpoint):
    client = StarWarsAPIClient()
    results = client.get_planets()
    assert len(results) == 3
    assert results[0] == {
        "name": "Geonosis",
        "rotation_period": "30",
        "orbital_period": "256",
        "diameter": "11370",
        "climate": "temperate, arid",
        "gravity": "0.9 standard",
        "terrain": "rock, desert, mountain, barren",
        "surface_water": "5",
        "population": "100000000000",
        "residents": ["https://swapi.dev/api/people/63/"],
        "films": ["https://swapi.dev/api/films/5/"],
        "created": "2014-12-10T12:47:22.350000Z",
        "edited": "2014-12-20T20:58:18.437000Z",
        "url": "https://swapi.dev/api/planets/11/",
    }


def test_get_people(mock_characters_endpoint):
    client = StarWarsAPIClient()
    results = client.get_characters()
    assert len(results) == 11
    assert results[0] == {
        "name": "Anakin Skywalker",
        "height": "188",
        "mass": "84",
        "hair_color": "blond",
        "skin_color": "fair",
        "eye_color": "blue",
        "birth_year": "41.9BBY",
        "gender": "male",
        "homeworld": "https://swapi.dev/api/planets/1/",
        "films": ["https://swapi.dev/api/films/4/", "https://swapi.dev/api/films/5/", "https://swapi.dev/api/films/6/"],
        "species": [],
        "vehicles": ["https://swapi.dev/api/vehicles/44/", "https://swapi.dev/api/vehicles/46/"],
        "starships": [
            "https://swapi.dev/api/starships/39/",
            "https://swapi.dev/api/starships/59/",
            "https://swapi.dev/api/starships/65/",
        ],
        "created": "2014-12-10T16:20:44.310000Z",
        "edited": "2014-12-20T21:17:50.327000Z",
        "url": "https://swapi.dev/api/people/11/",
    }
    assert results[-1].get("name") == "Boba Fett"
    assert results[-1].get("birth_year") == "31.5BBY"


def test_incorrect_fetch_mode():
    with pytest.raises(ValueError):
        StarWarsAPIClient(fetching_mode="unknown").get_characters()
