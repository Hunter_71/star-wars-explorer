import logging
import math
from concurrent.futures import ThreadPoolExecutor, as_completed
from enum import Enum
from typing import Dict, List

import requests
from retry import retry

logger = logging.getLogger("django")
logger.setLevel(logging.INFO)

# TODO: If any of below is planned to be frequently changed (eg. testing with different data sources),
#       below constants can be turned into configuration settings or env vars.
BASE_URL = "https://swapi.dev/api"
CHARACTERS_ENDPOINT = "people"
PLANETS_ENDPOINT = "planets"
PAGE_SIZE = 10
MAX_WORKERS = 4


class FetchingModeSWAPI(Enum):
    Sequential = "SEQUENTIAL"
    Parallel = "PARALLEL"


class StarWarsAPIClient:
    def __init__(self, url: str = BASE_URL, fetching_mode: FetchingModeSWAPI = FetchingModeSWAPI.Parallel):
        self.url = url
        self.mode = fetching_mode

    def get_characters(self) -> List:
        logger.info("Collecting characters from SWAPI")

        url = f"{self.url}/{CHARACTERS_ENDPOINT}"
        return self._get_all_data(url)

    def get_planets(self) -> List:
        logger.info("Collecting planets from SWAPI")

        url = f"{self.url}/{PLANETS_ENDPOINT}"
        return self._get_all_data(url)

    def _get_all_data(self, url: str) -> List:
        if self.mode == FetchingModeSWAPI.Parallel:
            results = self._get_all_data_parallel(url)
        elif self.mode == FetchingModeSWAPI.Sequential:
            results = self._get_all_data_sequential(url)
        else:
            raise ValueError(f"Not supported data fetching mode: {self.mode}")

        logger.info(f"Fetched {len(results)} results.")
        return results

    def _get_all_data_sequential(self, url: str) -> List:
        """
        This method allows to collect all data from given SWAPI endpoint sequential.
        The results would be sorted as they are in the API.
        """
        response = self._get_resource(url)
        results = response.get("results", [])

        while next_page_url := response.get("next", None):
            response = self._get_resource(url=next_page_url)
            results += response.get("results", [])

        return results

    def _get_all_data_parallel(self, url: str) -> List:
        """
        This method allows to collect all data from given SWAPI endpoint in a parallel.

        Note: the results may not be sorted due to API records order,
              as API request would not necessarily be processed in order.
              If the results ordering is critical, consider collecting data sequential.
        """
        response = self._get_resource(url)
        results = response.get("results", [])
        total = int(response.get("count", 0))
        pages = math.ceil(total / PAGE_SIZE)

        with ThreadPoolExecutor(max_workers=MAX_WORKERS) as executor:
            futures = [executor.submit(self._get_resource_by_page, url, page) for page in range(2, pages + 1)]
            for future in as_completed(futures):
                results += future.result()

        return results

    def _get_resource_by_page(self, url: str, page: int) -> List:
        return self._get_resource(f"{url}?page={page}").get("results", [])

    @staticmethod
    @retry(tries=3, delay=0.2, backoff=2)
    def _get_resource(url: str) -> Dict:
        logger.info(f"Requesting: {url}")

        response = requests.get(url)
        response.raise_for_status()
        return response.json()
